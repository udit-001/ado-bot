<p align="center">
<img src="https://cdn5.telesco.pe/file/YmBm-ODCbbKqf2OlAElYvK1lVgeZR3wBzVkZfLj9dbIj4qL-CBpuBRrgGxHHsQ5BYpF8qDsPUevJa-OOBWAkL5XeTx8LW3k9zNZi8ipysOWkf_d46tnbVdSyfGfu0mqMbtY-o9Fp6YmtQWFrOr6nrVZO7icuAvZeWxr1oyV0Kyk6y3xKC3K9GTZQksxu1N_SvDBmWKjFY6-QDXdd-K5rEUOKNB2kMviXrOexqHuvlqf0r-llSUTu9FuvmhzB8v9cxf9hGBGDrJvwcjU4PsDH5VoEwgwxlFkx0Gk9bCV0AMYIKs-rEC__E3pOp1QM30nsZsDsZEnN4GofMw-IOFxHUw.jpg" height="150px">
  </p>

<h2 align="center"> Ado-bot <img src="https://img.shields.io/website/https/evening-coast-69979.herokuapp.com?down_color=lightgrey&down_message=offline&label=bot&style=flat-square&up_color=blue&up_message=online" alt="Website Status">
<img src="http://hits.dwyl.io/udit-001/ado-bot.svg" alt="Hit Count">
</h2>

<p align="center">
  An Avatar Generation Telegram Bot, made using <a href="https://python-telegram-bot.org/" rel="noopener noreferrer">python-telegram-bot</a> library.
</p>
